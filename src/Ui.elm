module Ui exposing (..)

import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Octicons as Octicons



--COLORS
line myWidth rotation =
    el
        [ Border.width 2
        , Border.color palette.white
        , width (px myWidth)
        , rotate rotation
        ]
        none


type alias Palette =
    { black : Color
    , white : Color
    , blue : Color
    , grey : Color
    , yellow : Color
    }


palette : Palette
palette =
    { black = rgb255 0 0 0
    , white = rgb255 255 255 255
    , blue = rgb255 6 74 145
    , grey = rgb255 242 242 242
    , yellow = rgb255 255 204 51
    }


button { label, onPress } scaling =
    Input.button
        [ Font.color palette.white
        , Font.size 28
        , Font.bold
        , Font.family [ Font.typeface "Roboto" ]
        , mouseOver [ alpha 0.7 ]
        , scale scaling
        , focused []
        ]
        { label = label, onPress = onPress }


buttonNav { label, url } =
    link
        [
         mouseOver [ alpha 0.7 ]
        , padding 10
        ]
        { label = label, url = url }


backButton =
    link
        []
        { label = html (Octicons.defaultOptions |> Octicons.color "black" |> Octicons.size 80 |> Octicons.chevronLeft), url = "/" }


yellow =
    rgb255 255 204 51



card =
    Element.column
        [ width (px 300)
        , height (px 300)
        , Background.color palette.white
        , Border.shadow { offset = ( 0, 4 ), size = 0.5, blur = 6, color = palette.grey }
        ]
        [ picture
        , memberName "Christiane Acourt"
        , memberTitle "CEO"
        ]


memberName firstname =
    Element.el [ Font.size 26, mainFont, Font.bold, centerX, centerY ] (text firstname)


memberTitle title =
    Element.el [ centerX,centerY, padding 10, Font.size 18, mainFont, Font.bold, alpha 0.7 ] (text title)


picture =
    Element.image [ width (px 80), height (px 80), clip, Border.rounded 1000, centerX,  moveDown 30 ] { src = "/Media/picture-example.jpeg", description = "Picture of team's members" }



smallLogo =
    Element.image
        [ width (px 150), height (px 55) ]
        { src = "/Media/cnci-logo.png", description = "Logo cnci" }


arrowUp color msg =
    Input.button
        [focused[]]
        { label = Element.html (Octicons.defaultOptions |> Octicons.color color |> Octicons.size 50 |> Octicons.chevronUp)
        , onPress = Just msg
        }


arrowDown color msg =
    Input.button
        [focused[]]
        { label = Element.html (Octicons.defaultOptions |> Octicons.color color |> Octicons.size 50 |> Octicons.chevronDown)
        , onPress = Just msg
        }


mockedListOfClients customScale =
    List.repeat 5 <|
        Element.el [ Font.size 36, Font.color palette.white, Font.bold, mainFont, padding 30, scale customScale, centerX ] (text <| "DIOR      " ++ "·")



highlight element =
    Element.el [ Font.color palette.blue ] (text element)


yellowSeparator myWidth =
    Element.el [ Border.width 1, Border.color palette.yellow, width (px <| myWidth) ] none


dropDownLink { label, url } =
    link
        [ Font.color palette.black
        , width (px 150)
        , Font.size 21
        , Font.bold
        , paddingXY 20 20
        , Background.color palette.white
        , mouseOver [ Background.color palette.black, Font.color palette.white, Border.color palette.white ]
        ]
        { label = label, url = url }


arrowDownHome { label, onPress } =
    Input.button
        [
         padding 10
        , Background.color palette.blue
        , Border.rounded 100
        ]
        { label =
            label
        , onPress = onPress
        }



--FONTS


mainTitle : String -> Element.Color -> Element msg
mainTitle myText color =
    Element.el [ mainFont, paddingXY 40 100, Font.size 96, Font.color color, Font.bold ] (text <| String.toUpper myText)

titleTwo myText myColor =
    el
        [ Font.size 60
        , Font.color myColor
        , Font.bold
        , mainFont
        ]
        (text <| String.toUpper myText)

titleThree myText myColor =
    el
        [ Font.size 33
        , Font.color myColor
        , mainFont
        , Font.light
        ]
        (text <| String.toUpper myText)


mainFont =
    Font.family [ Font.typeface "Roboto" ]


viewTitle title color =
    Element.el [ mainFont, Font.size 76, Font.color color, Font.light ] (text <| String.toUpper title)

customParagraph paragraphWidth customScale listOfText =
    paragraph
        [ width (fill |> maximum 800), scale customScale, Font.size 32, paddingXY 0 30 ]
        listOfText

-- SYMBOLS


grabber : String -> Int -> Element msg
grabber color size =
    Element.html
        (Octicons.defaultOptions |> Octicons.color color |> Octicons.size size |> Octicons.grabber)


chevronUp color size =
    Element.html
        (Octicons.defaultOptions |> Octicons.color color |> Octicons.size size |> Octicons.chevronUp)


chevronDown color size =
    Element.html
        (Octicons.defaultOptions |> Octicons.color color |> Octicons.size size |> Octicons.chevronDown)



--Assets


coloredLogo customScale =
    Element.image
        [ width (px 676)
        , height (px 251)
        , alpha 0.8
        , scale customScale
        ]
        { src = "/Media/cnci-logo-colored.png", description = "Logo cnci" }

smallColoredLogo customScale =
    Element.image
        [ width (px 135)
        , height (px 50)
        , alpha 0.8
        , scale customScale
        ]
        { src = "/Media/cnci-logo-colored.png", description = "Logo cnci" }
--SEPARATORS
topLine =
  row []
    [ el [ paddingEach { top = 0, right = 0, bottom = 0, left = 60 } ] <| line 500 0
    , el [ rotate 3, moveLeft 19, moveDown 38 ] <| line 100 1
    ]
