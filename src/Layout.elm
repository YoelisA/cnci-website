module Layout exposing (view)

import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Generated.Routes as Routes exposing (Route, routes)
import Ui exposing (..)
import Utils.Spa as Spa


view : Spa.LayoutContext msg -> Element msg
view { page, route } =
    column [ width fill ]
        [ page
        ]


--viewHeader : Route -> Element msg
--viewHeader currentRoute =
--    row
--        [ width fill
--        , Font.family [ Font.typeface "'Roboto'" ]
--        , spacing 30
--        , paddingXY 20 20
--        , Font.bold
--        , Font.size 28
--        , Background.color Ui.black
--        ]
--        [ Element.el [ width (px 300), height (px 55), --Background.color Ui.white ] (Element.text "logo blanc")
--        , row [ alignRight ]
--            []
--        ]
--

viewLogo =
    Element.image [] { src = "/Media/cnci-logo.png", description = "white logo" }


viewLink : Route -> ( String, Route ) -> Element msg
viewLink currentRoute ( label, route ) =
    if currentRoute == route then
        el
            [ Font.underline
            , Font.color Ui.palette.white
            , Font.size 16
            , paddingXY 20 20
            ]
            (text label)

    else
        link
            [ Font.color Ui.palette.white
            , Font.size 16
            , mouseOver [ alpha 0.5 ]
            , paddingXY 20 20
            ]
            { label = text label
            , url = Routes.toPath route
            }
