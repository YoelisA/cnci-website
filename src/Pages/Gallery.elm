module Pages.Gallery exposing (Model, Msg, page)

import Browser.Events as Bevents
import Ease exposing (..)
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Events as Events
import Element.Font as Font
import Element.Input as Input
import Generated.Params as Params
import Generated.Routes as Routes exposing (Route, routes)
import Html as Html
import Html.Attributes as Attr
import Json.Decode as Decode
import Octicons exposing (arrowDown, chevronLeft, defaultOptions)
import ScrollTo exposing (..)
import Spa.Page
import Ui exposing (..)
import Utils.Spa as Spa exposing (Page)
import Browser.Dom as Dom
import Task as Task


page : Page Params.Gallery Model Msg model msg appMsg
page =
    Spa.Page.element
        { title = always "Gallery"
        , init = always init
        , update = always update
        , subscriptions = always subscriptions
        , view = always view
        }



-- INIT


type alias Model =
    { windowWidth : Int
    , windowHeight : Int
    }


init : Params.Creations -> ( Model, Cmd Msg )
init _ =
    ( {windowWidth = 1280
    , windowHeight = 800
      }
    , Task.perform (\a-> GotWindowSize a ) Dom.getViewport
    )



-- UPDATE


type Msg
    = GotNewWindowSize Int Int
    | GotWindowSize Dom.Viewport


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GotNewWindowSize newWidth newHeight ->
            ( { model | windowWidth = newWidth }, Cmd.none )
        GotWindowSize viewPort ->
          ({ model | windowHeight = round viewPort.viewport.height, windowWidth = round viewPort.viewport.width}, Cmd.none)



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch [ Bevents.onResize (\w h -> GotNewWindowSize w h) ]



-- VIEW


view : Model -> Element Msg
view model =
    let
        device =
            classifyDevice { height = model.windowHeight, width = model.windowWidth }
    in
    case device.class of
        BigDesktop ->
            column
                [ Background.color Ui.palette.black
                , width fill
                , height <| px 1080
                ]
                [ row
                    [ Background.color Ui.palette.black
                    , width fill
                    , height (px 100)
                    , Border.shadow { offset = ( -1, -1 ), size = 2, blur = 10, color = Ui.palette.black }
                    ]
                    [ el [ padding 30 ] <| Ui.smallLogo ]
                , row []
                    [ el
                        [ paddingEach
                            { top = 0
                            , left = 0
                            , right = 10
                            , bottom = 0
                            }
                        ]
                      <|
                        backButton
                    , Ui.viewTitle "Productions audiovisuelles" Ui.palette.white
                    ]
                , viewCards
                ]

        Phone ->
          viewGallery 0.4

        _ ->
          column
              [ Background.color Ui.palette.black
              , width fill
              , height <| px 1080
              ]
              [ row
                  [ Background.color Ui.palette.black
                  , width fill
                  , height (px 100)
                  , Border.shadow { offset = ( -1, -1 ), size = 2, blur = 10, color = Ui.palette.black }
                  ]
                  [ el [ padding 30 ] <| Ui.smallLogo ]
              , row []
                  [ el
                      [ paddingEach
                          { top = 0
                          , left = 0
                          , right = 10
                          , bottom = 0
                          }
                      ]
                    <|
                      backButton
                  , Ui.viewTitle "Productions audiovisuelles" Ui.palette.white
                  ]
              , viewCards
              ]

viewGallery customScale =
  column
    [ Background.color Ui.palette.black
    , width fill
    , height fill
    ]
    [ row
        [ Background.color Ui.palette.black
        , width fill
        , height fill
        , Border.shadow { offset = ( -1, -1 ), size = 2, blur = 10, color = Ui.palette.black }
        ]
        [ el [ padding 30 ] <| Ui.smallLogo ]
    , row []
        [ el
            [ paddingEach
                { top = 0
                , left = 0
                , right = 10
                , bottom = 0
                }
              , scale customScale
            ]
          <|
            backButton
        , el [Ui.mainFont, Font.light, Font.color Ui.palette.white, Font.size 21](text ("créations de services" |> String.toUpper))
        ]
    , viewCards
    ]

backButton =
    link
        []
        { label = html (Octicons.defaultOptions |> Octicons.color "white" |> Octicons.size 80 |> Octicons.chevronLeft), url = "/" }


viewCards =
    let
        emptyCards =
            List.repeat 6 card
    in
          emptyCards |> wrappedRow [ centerX, width (fill |> maximum 1200), spacing 80, padding 30 ]



card =
    Element.column
        [ width (px 300)
        , height (px 300)
        , Background.color Ui.palette.white
        , Border.glow Ui.palette.white 2
        , Background.image "/Media/zesto.png"
        ]
        [ cardMoreInfo ]


cardMoreInfo =
    Element.column
        [ width <| px 280
        , height <| px 280
        , Background.color Ui.palette.black
        , alpha 0
        , mouseOver [ alpha 0.7 ]
        , centerX
        , centerY
        ]
        [ el [ Font.color Ui.palette.white, Ui.mainFont, Font.bold, Font.size 36, centerX, centerY ] (text "ZESTO") ]
