module Pages.Top exposing (Model, Msg, page)

import Browser.Dom as Dom
import Browser.Events as Bevents
import Ease exposing (..)
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Events as Events
import Element.Font as Font
import Element.Input as Input
import Generated.Params as Params
import Generated.Routes as Routes exposing (Route, routes)
import Global exposing (..)
import Html as Html
import Html.Attributes as Attr
import Json.Decode as Decode
import Octicons exposing (arrowDown, defaultOptions)
import ScrollTo exposing (..)
import Spa.Page
import Task as Task
import Ui exposing (..)
import Utils.Spa as Spa exposing (Page)



--INIT


page : Page Params.Top Model Msg model msg appMsg
page =
    Spa.Page.element
        { title = always "Landing"
        , init = always init
        , update = always update
        , subscriptions = always subscriptions
        , view = always view
        }


type alias Model =
    { scroll : ScrollTo.Status
    , senderName : String
    , senderSecondName : String
    , numberOfScrolls : Int
    , isDropdownVisible : Bool
    , isHamDropdownVisible : Bool
    , windowWidth : Int
    , windowHeight : Int
    }


init _ =
    ( { scroll = ScrollTo.init |> ScrollTo.withDuration 800 |> ScrollTo.withEasing Ease.inOutBack
      , senderName = ""
      , senderSecondName = ""
      , numberOfScrolls = 0
      , isDropdownVisible = False
      , isHamDropdownVisible = False
      , windowWidth = 1280
      , windowHeight = 800
      }
    , Task.perform (\a -> GotWindowSize a) Dom.getViewport
    )



--UPDATE


type Msg
    = ClickScrollTo Panel
    | ClickScrollDown
    | ClickScrollUp
    | ScrollMsg ScrollTo.Msg
    | Update Model
    | GotInput UserAction
    | MenuDropdown Bool
    | HamburgerDropdown Bool
    | GotNewWindowSize Int Int
    | GotWindowSize Dom.Viewport


type alias Position =
    Int


type Panel
    = Top
    | Activity
    | Group
    | Client
    | Equipe
    | Contact


panelToInt : Panel -> Int
panelToInt panel =
    case panel of
        Top ->
            0

        Activity ->
            1

        Group ->
            2

        Client ->
            3

        Equipe ->
            4

        Contact ->
            5



--TODO : Try to replace scrollToPosition with scrollToKey. Advantage : it will reduce the size of the update function.


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        updateGiven viewPortPos =
            ( { model | numberOfScrolls = viewPortPos }
            , Cmd.map ScrollMsg <| ScrollTo.toPosition { x = 0, y = toFloat model.windowHeight * toFloat viewPortPos } model.scroll
            )
    in
    case msg of
        ClickScrollTo panel ->
            case panel of
                Top ->
                    updateGiven <| panelToInt Top

                Activity ->
                    updateGiven <| panelToInt Activity

                Group ->
                    updateGiven <| panelToInt Group

                Client ->
                    updateGiven <| panelToInt Client

                Equipe ->
                    updateGiven <| panelToInt Equipe

                Contact ->
                    updateGiven <| panelToInt Contact

        ClickScrollDown ->
            if model.numberOfScrolls == 0 then
                ( { model
                    | numberOfScrolls = clamp 0 5 <| model.numberOfScrolls + 1
                  }
                , Cmd.map ScrollMsg <| ScrollTo.toPosition { x = 0, y = toFloat model.windowHeight } model.scroll
                )

            else
                ( { model
                    | numberOfScrolls = clamp 0 5 <| model.numberOfScrolls + 1
                  }
                , Cmd.map ScrollMsg <| ScrollTo.toPosition { x = 0, y = toFloat model.windowHeight * toFloat model.numberOfScrolls } model.scroll
                )

        ClickScrollUp ->
            ( { model
                | numberOfScrolls = clamp 0 5 <| model.numberOfScrolls - 1
              }
            , Cmd.map ScrollMsg <| ScrollTo.toPosition { x = 0, y = toFloat model.windowHeight * toFloat model.numberOfScrolls } model.scroll
            )

        ScrollMsg scrollMsg ->
            Tuple.mapBoth
                (\status -> { model | scroll = status })
                (Cmd.map ScrollMsg)
                (ScrollTo.update scrollMsg model.scroll)

        Update newModel ->
            ( newModel, Cmd.none )

        GotInput key ->
            case key of
                ScrollDown ->
                    ( { model
                        | numberOfScrolls = clamp 0 4 <| model.numberOfScrolls + 1
                      }
                    , Cmd.map ScrollMsg <| ScrollTo.toPosition { x = 0, y = toFloat model.windowHeight * toFloat model.numberOfScrolls } model.scroll
                    )

                ScrollUp ->
                    ( { model | numberOfScrolls = clamp 0 4 <| model.numberOfScrolls - 1 }
                    , Cmd.map ScrollMsg <| ScrollTo.toPosition { x = 0, y = toFloat model.windowHeight * toFloat model.numberOfScrolls } model.scroll
                    )

        MenuDropdown isVisible ->
            case isVisible of
                True ->
                    ( { model | isDropdownVisible = True }, Cmd.none )

                False ->
                    ( { model | isDropdownVisible = False }, Cmd.none )

        HamburgerDropdown isVisible ->
            case isVisible of
                True ->
                    ( { model | isHamDropdownVisible = True }, Cmd.none )

                False ->
                    ( { model | isHamDropdownVisible = False }, Cmd.none )

        GotNewWindowSize newWidth newHeight ->
            ( { model | windowWidth = newWidth }, Cmd.none )

        GotWindowSize viewPort ->
            ( { model | windowHeight = round viewPort.viewport.height, windowWidth = round viewPort.viewport.width }, Cmd.none )



-- VIEW


view : Model -> Element Msg
view model =
    let
        device =
            classifyDevice { height = model.windowHeight, width = model.windowWidth }
    in
    case device.class of
        BigDesktop ->
            column
                [ Background.color Ui.palette.white
                , width fill
                ]
                [ viewHeader
                    { fontScale = 1
                    , model = model
                    , logoScale = 1
                    , viewSmallLogo = False
                    }
                , viewActivity model 1 False
                , viewGroup model 1.0 False
                , viewClients model 1.0
                , viewEquipe 1

                --, viewContact model
                , el [ Font.color Ui.palette.black, centerX, alignBottom ] <| Ui.button { label = text "To the Top", onPress = Just <| ClickScrollTo Top } 0.8
                ]

        Desktop ->
            column
                [ Background.color Ui.palette.white
                , width fill
                ]
                [ viewHeader
                    { fontScale = 1.0
                    , model = model
                    , logoScale = 1.0
                    , viewSmallLogo = True
                    }
                , viewActivity model 1 False
                , viewGroup model 1 False
                , viewClients model 1
                , viewEquipe 1

                --, viewContact model
                , el [ Font.color Ui.palette.black, centerX, alignBottom ] <| Ui.button { label = text "To the Top", onPress = Just <| ClickScrollTo Top } 0.8
                ]

        Tablet ->
            column
                [ Background.color Ui.palette.white
                , width fill
                ]
                [ viewHeader
                    { fontScale = 1
                    , model = model
                    , logoScale = 1
                    , viewSmallLogo = False
                    }
                , viewActivity model 1 False
                , viewGroup model 1 False
                , viewClients model 1
                , viewEquipe 1

                --, viewContact model
                , el [ Font.color Ui.palette.black, centerX, alignBottom ] <| Ui.button { label = text "To the Top", onPress = Just <| ClickScrollTo Top } 0.8
                ]

        Phone ->
            column
                [ Background.color Ui.palette.white
                , width fill
                ]
                [ viewHeaderPhone
                    { fontScale = 0.4
                    , model = model
                    , logoScale = 0.6
                    , viewSmallLogo = False
                    }
                , viewActivity model 0.7 True
                , viewGroup model 0.7 True
                , viewClients model 0.7
                , viewEquipe 0.7

                --, viewContact model
                , Ui.button { label = text "To the Top", onPress = Just <| ClickScrollTo Top } 0.6
                ]



--Phone


hamburger : Model -> Element Msg
hamburger model =
    let
        listOfSections =
            [ el [ centerX, paddingXY 0 20 ] <| Ui.button { label = text (String.toUpper "activités"), onPress = Just <| ClickScrollTo Activity } 0.8
            , el [ centerX, paddingXY 0 20 ] <| Ui.button { label = text (String.toUpper "Groupe"), onPress = Just <| ClickScrollTo Group } 0.8
            , el [ centerX, paddingXY 0 20 ] <| Ui.button { label = text (String.toUpper "Clients"), onPress = Just <| ClickScrollTo Client } 0.8
            , el [ centerX, paddingXY 0 20 ] <| Ui.button { label = text (String.toUpper "équipe"), onPress = Just <| ClickScrollTo Equipe } 0.8
            , el [ centerX, paddingXY 0 20 ] <| Ui.button { label = text (String.toUpper "Contact"), onPress = Just <| ClickScrollTo Contact } 0.8
            ]
    in
    if model.isHamDropdownVisible then
        Element.el
            [ centerX
            , Events.onClick <| HamburgerDropdown False
            , below <| column [ centerX, paddingXY 0 10 ] listOfSections
            ]
        <|
            Ui.grabber
                "white"
                80

    else
        Element.el
            [ centerX
            , Events.onClick <| HamburgerDropdown True
            ]
        <|
            Ui.grabber
                "white"
                80



--ACTIVITY


viewActivity model customScale phoneMode =
    if phoneMode then
        column
            [ width fill
            , height (px <| model.windowHeight)
            , clip
            , Background.image "/Media/activite-2.png"
            ]
            [ el
                [ scale customScale
                , padding 20
                ]
                (Ui.titleTwo "activités" Ui.palette.white)
            , column [ centerX ]
                [ el
                    [ scale customScale
                    , centerX
                    , paddingXY 0 40
                    , Font.bold
                    , Font.underline
                    , Font.color Ui.palette.white
                    ]
                    (Ui.buttonNav { label = Ui.titleThree "Créations de services" Ui.palette.white, url = "/creations" })
                , el
                    [ scale customScale
                    , centerX
                    , paddingXY 0 100
                    , Font.bold
                    , Font.underline
                    , Font.color Ui.palette.white
                    ]
                    (Ui.buttonNav { label = Ui.titleThree "PRODUCTIONS AUDIOVISUELLES" Ui.palette.white, url = "/gallery" })
                ]
            , row
                [ centerX
                , alignBottom
                , paddingXY 0 30
                ]
                [ Ui.arrowDown "white" (ClickScrollTo Group)
                , Ui.arrowUp "white" (ClickScrollTo Top)
                ]
            ]

    else
        column
            [ width fill
            , height (px <| model.windowHeight)
            , clip
            , Background.image "/Media/activite-2.png"
            ]
            [ el
                [ scale customScale
                , padding 20
                ]
                (Ui.titleTwo "activités" Ui.palette.white)
            , column []
                [ row []
                    [ el [ paddingEach { top = 0, right = 0, bottom = 0, left = 60 } ] <| Ui.line 500 0
                    , el [ rotate 3, moveLeft 18, moveDown 38 ] <| Ui.line 100 1
                    ]
                , el
                    [ alignLeft
                    , alignTop
                    , paddingXY 40 0
                    , mouseOver
                        [ alpha 0.7 ]
                    ]
                    (Ui.buttonNav { label = Ui.titleThree "Créations de services" Ui.palette.white, url = "/creations" })
                ]
            , column
                [ alignRight, alignBottom ]
                [ el
                    [ alignRight
                    , paddingXY 60 0
                    ]
                    (Ui.buttonNav { label = Ui.titleThree "PRODUCTIONS AUDIOVISUELLES" Ui.palette.white, url = "/gallery" })
                , el [ moveUp 38 ] <| Ui.line 100 1
                , el [ alignRight, paddingEach { top = 0, right = 54, bottom = 0, left = 0 } ] <| Ui.line 500 0
                ]
            , row
                [ centerX
                , alignBottom
                , paddingXY 0 30
                ]
                [ Ui.arrowDown "white" (ClickScrollTo Group)
                , Ui.arrowUp "white" (ClickScrollTo Top)
                ]
            ]



--GROUP


viewGroup model customScale phoneMode =
    if phoneMode then
        column
            [ Background.image "/Media/GROUPE.png"
            , width fill
            , height (px <| model.windowHeight)
            , clip
            , centerY
            ]
            [ el [ scale customScale, padding 20 ]
                (Ui.titleTwo "Groupe" Ui.palette.black)
            , textColumn
                [ centerX ]
                [ Ui.customParagraph model.windowWidth
                    customScale
                    [ text """Fondé en XXX, """
                    , Ui.highlight "CNCI Services"
                    , text """ accompagne ses clients dans la conception et création des services ainsi que de production audiovisuelles. """
                    ]
                , Ui.yellowSeparator <| model.windowWidth
                , Ui.customParagraph model.windowWidth
                    customScale
                    [ text """Connu pour sa créativité, son offre technologique et son efficacité opérationnelle, """
                    , Ui.highlight "CNCI Services"
                    , text """ est particulièrement spécialisé dans les univers de l’automobile, de la banque et l’assurance."""
                    ]
                ]
            , row
                [ moveUp 100
                , centerX
                ]
                [ Ui.arrowDown "black" (ClickScrollTo Client)
                , Ui.arrowUp "black" (ClickScrollTo Activity)
                ]
            ]

    else
        column
            [ Background.image "/Media/GROUPE.png"
            , width fill
            , height (px <| model.windowHeight)
            , clip
            , centerY
            ]
            [ el [ scale customScale, padding 20 ]
                (Ui.titleTwo "Groupe" Ui.palette.black)
            , textColumn
                [ alignTop, padding 40 ]
                [ Ui.customParagraph model.windowWidth
                    customScale
                    [ text """Fondé en XXX, """
                    , Ui.highlight "CNCI Services"
                    , text """ accompagne ses clients dans la conception et création des services ainsi que de production audiovisuelles. """
                    ]
                , Ui.yellowSeparator <| model.windowWidth // 2
                , Ui.customParagraph model.windowWidth
                    customScale
                    [ text """Connu pour sa créativité, son offre technologique et son efficacité opérationnelle, """
                    , Ui.highlight "CNCI Services"
                    , text """ est particulièrement spécialisé dans les univers de l’automobile, de la banque et l’assurance."""
                    ]
                ]
            , row
                [ moveUp 100
                , centerX
                ]
                [ Ui.arrowDown "black" (ClickScrollTo Client)
                , Ui.arrowUp "black" (ClickScrollTo Activity)
                ]
            ]



-- CLIENTS


viewClients model customScale =
    column
        [ Background.image "/Media/CLIENTS.png", width fill, height (px <| model.windowHeight), clip ]
        [ el [ scale customScale, padding 20 ] (Ui.titleTwo "CLIENTS" Ui.palette.black)
        , row [ centerX, alignBottom ] [ Ui.arrowDown "black" (ClickScrollTo Equipe), arrowUp "black" (ClickScrollTo Group) ]
        , row
            [ width fill
            , height <| px <| model.windowHeight // 8
            , alignBottom
            , Background.color Ui.palette.black
            ]
          <|
            Ui.mockedListOfClients customScale
        ]


viewHeader :
    { model : Model
    , fontScale : Float
    , logoScale : Float
    , viewSmallLogo : Bool
    }
    -> Element Msg
viewHeader { model, fontScale, logoScale, viewSmallLogo } =
    row
        [ width fill
        , height (px model.windowHeight)
        , clip
        , behindContent <|
            column [ centerX, centerY ]
                [ viewMovie model fontScale 0 False
                ]
        , inFront
            <|el
                [ alignBottom, centerX, alpha 0.9 ]
              <|
                Ui.arrowDownHome
                    { label =
                        Ui.chevronDown "white" 40
                    , onPress = Just <| ClickScrollTo Activity
                    }

        , alignRight
        ]
        [ Element.el
            [ scale logoScale
            , alignTop
            , paddingEach { top = 10, right = 100, bottom = 0, left = 20 }
            ]
            Ui.smallLogo
        , el
            [ alignTop, alignRight ]
            (row
                [ spacing 30
                , scale fontScale
                , width fill
                , paddingEach { top = 30, right = 30, bottom = 0, left = 0 }
                ]
                [ viewActivityLink model.isDropdownVisible model fontScale
                , Ui.button { label = text (String.toUpper "Groupe"), onPress = Just <| ClickScrollTo Group } fontScale
                , Ui.button { label = text (String.toUpper "Clients"), onPress = Just <| ClickScrollTo Client } fontScale
                , Ui.button { label = text (String.toUpper "équipe"), onPress = Just <| ClickScrollTo Equipe } fontScale

                --, Ui.button { label = text (String.toUpper "Contact"), onPress = Just <| ClickScrollTo Contact } fontScale
                ]
            )
        ]


viewHeaderPhone { model, fontScale, logoScale, viewSmallLogo } =
    column
        [ width fill
        , height (px model.windowHeight)
        , clip
        , behindContent <| viewMovie model fontScale 30 True
        , alignRight
        ]
        [ hamburger model
        ]


viewActivityLink visible model customScale =
    if visible then
        Element.el
            [ below <| viewDropdown model
            , Events.onMouseLeave <| MenuDropdown False
            ]
            (Ui.button
                { label =
                    row []
                        [ text "ACTIVITÉS"
                        , Ui.chevronUp "white" 20
                        ]
                , onPress = Just <| ClickScrollTo Activity
                }
                customScale
            )

    else
        Element.el
            [ Events.onMouseEnter <| MenuDropdown True
            ]
            (Ui.button
                { label =
                    row []
                        [ text "ACTIVITÉS"
                        , Ui.chevronDown "white" 20
                        ]
                , onPress = Just <| ClickScrollTo Activity
                }
                customScale
            )


viewDropdown : Model -> Element Msg
viewDropdown model =
    column [ paddingXY 0 10 ]
        [ Ui.dropDownLink { label = text "Créations", url = "/creations" }
        , Ui.dropDownLink { label = text "Gallerie", url = "/gallery" }
        ]


viewMovie : Model -> Float -> Float -> Bool -> Element Msg
viewMovie model customScale correctPos phoneMode =
    case phoneMode of
        False ->
            column
                [ height (px model.windowHeight), width fill ]
                [ Element.el
                    [ centerX
                    , alignTop
                    ]
                  <|
                    Element.el [ centerX, clip, Background.color Ui.palette.black, width (px model.windowWidth), height (px model.windowHeight) ]
                        (html
                            (Html.video
                                [ Attr.src "/Media/video.webm", Attr.autoplay True, Attr.loop True ]
                                []
                            )
                        )
                ]

        True ->
            column
                [ Background.color Ui.palette.black, height (px model.windowHeight), width (px model.windowWidth), clip ]
                [ Element.el
                    [ centerX
                    , centerY
                    ]
                  <|
                    Ui.smallColoredLogo 2
                , el [ alignBottom, centerX, alpha 0.9 ] <|
                    Ui.arrowDownHome
                        { label =
                            Ui.chevronDown "white" 40
                        , onPress = Just <| ClickScrollTo Activity
                        }
                ]


viewEquipe customScale =
    column
        [ Background.image "/Media/EQUIPE.png", width fill, height (px <| 1080), clipX ]
        [ el [ scale customScale, padding 20 ] <| Ui.titleTwo "équipe" Ui.palette.black
        , viewCards
        , row [ centerX, alignBottom, moveUp 80 ] [ Ui.arrowDown "black" (ClickScrollTo Contact), Ui.arrowUp "black" (ClickScrollTo Client) ]
        ]


viewCards =
    let
        emptyCards =
            List.repeat 6 Ui.card
    in
    emptyCards |> wrappedRow [ centerX, padding 30, width (fill |> maximum 1200), spacing 80 ]



--viewContact : Model -> Element Msg
--viewContact model =
--    column
--        [ Background.color Ui.palette.grey, width fill, height (px <| model.windowHeight) ]
--        [  "Contact" Ui.palette.white
--        , viewCallUs
--        , Input.username
--            []
--            { text = model.senderName
--            , placeholder = Just (Input.placeholder [] (text "nom"))
--            , onChange = \new -> Update { model | senderName = new }
--            , label = Input.labelAbove [ Font.size 14 ] (text "Votre nom")
--            }
--        , Input.username
--            []
--            { text = model.senderSecondName
--            , placeholder = Just (Input.placeholder [] (text "Prénom"))
--            , onChange = \new -> Update { model | senderSecondName = new }
--            , label = Input.labelAbove [ Font.size 14 ] (text "Votre prénom")
--            }
--        , viewArrowUp "black" ClickScrollToTop
--        ]
--viewCallUs =
--    Element.paragraph
--        [ width (fill |> maximum 1200), moveRight 40 ]
--        [ Element.el [ Font.bold, Font.size 55 ] (text "Appelez-nous ")
--        , Element.el [ Font.bold, Font.color yellow, Font.size 45 ] (text "0969329497")
--        , Element.el [ Font.size 23, alpha 0.5 ] (text "(numéro cristal, appel non-surtaxé)")
--        , Element.el [ Font.size 25 ] (text "du")
--        , Element.el [ Font.bold, Font.size 25 ] (text "LUNDI AU VENDREDI")
--        , Element.el [ Font.size 25 ] (text "DE")
--        , Element.el [ Font.bold, Font.size 25 ] (text "9h00 à 12H30")
--        , Element.el [ Font.size 25 ] (text "ET DE")
--        , Element.el [ Font.bold, Font.size 25 ] (text "14H00 À 18H00")
--        ]
--
--
--viewForm =
--    Element.column
--        []
--        [--firstLine
--        ]
--


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ ScrollTo.subscriptions ScrollMsg model.scroll
        , Bevents.onKeyDown (Decode.map GotInput keyDecoder)
        , Bevents.onResize (\w h -> GotNewWindowSize w h)
        ]


type UserAction
    = ScrollDown
    | ScrollUp


keyDecoder : Decode.Decoder UserAction
keyDecoder =
    Decode.field "key" Decode.string
        |> Decode.andThen keyToUserAction


keyToUserAction : String -> Decode.Decoder UserAction
keyToUserAction keyString =
    case keyString of
        "ArrowDown" ->
            Decode.succeed ScrollDown

        "ArrowUp" ->
            Decode.succeed ScrollUp

        _ ->
            Decode.fail "Not an event we care about"
