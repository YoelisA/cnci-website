module Global exposing
    ( Flags
    , Model
    , Msg(..)
    , init
    , subscriptions
    , update
    , windowSize
    )

import Generated.Routes as Routes exposing (Route)
import Ports

type alias Flags =
   {windowWidth : Int}



type alias Model =
    {windowWidth : Int}


windowSize model = {width = 30, height = 30}

type Msg
    = Msg


type alias Commands msg =
    { navigate : Route -> Cmd msg
    }


init : Commands msg -> Flags -> ( Model, Cmd Msg, Cmd msg )
init _ flags =
    ( { windowWidth = flags.windowWidth}
    , Cmd.none
    , Ports.log "Hello!"
    )


update : Commands msg -> Msg -> Model -> ( Model, Cmd Msg, Cmd msg )
update _ _ model =
    ( model
    , Cmd.none
    , Cmd.none
    )


subscriptions : Model -> Sub Msg
subscriptions _ =
    Sub.none
