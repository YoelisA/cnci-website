module Generated.Route exposing
    ( Route(..)
    , toPath
    )

import Generated.Params as Params


type Route
    = Creations Params.Creations
    | Gallery Params.Gallery
    | NotFound Params.NotFound
    | Top Params.Top


toPath : Route -> String
toPath route =
    case route of
        Creations _ ->
            "/creations"
        
        
        Gallery _ ->
            "/gallery"
        
        
        NotFound _ ->
            "/not-found"
        
        
        Top _ ->
            "/"