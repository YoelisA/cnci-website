module Generated.Routes exposing
    ( Route
    , parsers
    , routes
    , toPath
    )

import Generated.Route
import Url.Parser as Parser exposing ((</>), Parser, map, s, string, top)



-- ALIASES


type alias Route =
    Generated.Route.Route


toPath : Route -> String
toPath =
    Generated.Route.toPath



-- ROUTES


type alias Routes =
    { creations : Route
    , gallery : Route
    , notFound : Route
    , top : Route
    }


routes : Routes
routes =
    { creations =
        Generated.Route.Creations {}
    , gallery =
        Generated.Route.Gallery {}
    , notFound =
        Generated.Route.NotFound {}
    , top =
        Generated.Route.Top {}
    }
 

parsers : List (Parser (Route -> a) a)
parsers =
    [ map routes.creations
        (s "creations")
    , map routes.gallery
        (s "gallery")
    , map routes.notFound
        (s "not-found")
    , map routes.top
        (top)
    ]